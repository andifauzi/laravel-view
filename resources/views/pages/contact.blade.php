@extends('layouts.app')

@section('content')
<div id="testimonials" class="bg-blue-100 pt-16">
    <div class="mt-16">
        <div class="bg-blue-500 pt-16 pb-16 md:pb-32 relative">
            <img src="{{url("/images/wave3.svg")}}"
                class="w-full absolute bottom-full h-16 lg:h-auto object-cover object-top">
            <div class="container px-6 mx-auto">
                <div class="md:w-2/3 mx-auto relative">
                    <img src="{{url("/images/quote.svg")}}" class="absolute top-0 left-0 sm:-ml-16 -mt-4">
                    <h3 class="text-white italic text-2xl text-center">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam imperdiet est tellus, et consequat
                        sem sodales id. Quisque molestie et mauris efficitur lacinia.
                        <strong class="block not-italic mt-6 text-primary">FRANK</strong>
                    </h3>
                </div>
            </div>
        </div>
        <div class="flex items-center justify-center -mt-8 md:-mt-16 lg:-mt-24 relative">

            @foreach ($images as $image)
            @if ($loop->index == 2)
            <a href="#">
                <img src="{{url($image)}}"
                    class="w-16 h-16 md:w-32 md:h-32 lg:w-48 lg:h-48 mx-2 lg:mx-4 object-cover rounded-full border-2 md:border-4 border-white">
            </a>
            @else

            <a href="#">
                <img src="{{url($image)}}"
                    class="w-12 h-12 md:w-24 md:h-24 lg:w-32 lg:h-32 mx-2 lg:mx-4 object-cover rounded-full border-2 md:border-4 border-white">
            </a>
            @endif

            @endforeach
        </div>
    </div>
    <div id="contact" class="container mx-auto px-6 py-24">
        <h3 class="flex flex-col items-center text-4xl text-secondary font-bold mb-12">I need more info! <span
                class="bg-primary h-1 w-20 block mt-4"></span></h3>
        <div class="flex shadow-lg md:w-2/3 lg:w-1/2 xl:w-2/5 p-1 rounded-full overflow-hidden mx-auto bg-white">
            <input type="text" name="" placeholder="Insert your mail"
                class="h-16 text-secondary-700 w-64 flex-1 px-8 text-lg focus:outline-none">
            <button class="bg-primary w-32 uppercase font-bold text-secondary rounded-full">Send</button>
        </div>
    </div>
</div>
@endsection
